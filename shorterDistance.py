import json
import requests

#samuel.santiago170@gmail.com
#publioufc@alu.ufc.br
#caiofera35@gmail.com

# servers
mainServer = "http://34.226.142.186:3000"
helperServer = "http://34.227.105.219:8080"

# headers
headers = {
	'Content-type': 'application/json',
	'token': 'd787a91c-8495-4169-b993-9f29e3debcca'
}

# caminha para um 'stop'
def sendCoordinates(lat, lon):
	global mainServer, headers

	coordinates = {
		'lat': lat,
		'long': lon,
		'teamId': 5
	}
	headers = {
		'Content-type': 'application/json',
		'token': "d787a91c-8495-4169-b993-9f29e3debcca"
	}

	data = json.dumps(coordinates)
	response = requests.post(mainServer + "/coordinates", data=data, headers=headers)

	return response

# retorna todas as paradas
def getStops():
	global mainServer

	response = requests.get(mainServer + "/stops")
	stops = json.loads(response.content)
	
	return stops

# 'stop' mais proximo de uma coordenada
def getShorterDistance(lat, lng, stops):
	shorterDistance = getRealDistance(lat, lng, stops[0]['lat'], stops[0]['long'])
	thisStop = stops[0]

	for stop in stops[1:]:
		distance = getRealDistance(lat, lng, stop['lat'], stop['long'])
		if (distance < shorterDistance):
			shorterDistance = distance
			thisStop = stop
	return thisStop

# 'stop' mais distante de uma coordenada
def getGreaterDistance(lat, lng, stops):
	greaterDistance = getRealDistance(lat, lng, stops[0]['lat'], stops[0]['long'])
	thisStop = stops[0]

	for stop in stops[1:]:
		distance = getRealDistance(lat, lng, stop['lat'], stop['long'])
		if (distance > greaterDistance):
			greaterDistance = distance
			thisStop = stop
	return thisStop

# distancia real entre duas coordenadas
def getRealDistance(latStart, longStart, latEnd, longEnd):
	global mainServer, headers

	coordinates = {
		'coordinateStart': {
			'lat': latStart,
			'lng': longStart
		},
		'coordinateEnd': {
			'lat': latEnd,
			'lng': longEnd
		}
	}

	data = json.dumps(coordinates)
	
	response = requests.post(helperServer + "/Coordinate/Distance", data=data, headers=headers)
	return response.content

# interpolar trajeto
def interpolate(latS, lgnS, latE, lgnE, percent):
	coordinates = {
		"coordinateStart": {
			"lat": latS,
			"lng": lgnS
		},
		"coordinateEnd": {
			"lat": latE,
			"lng": lgnE
		},
		"percent": percent
	}

	data = json.dumps(coordinates)
	
	response = requests.post(helperServer + "/Coordinate/Position", data=data, headers=headers)
	return response.content

# magica
def proceed(latS, lngS, latM, lngM, latE, lngE):
	if(latE > latM > latS):
		return True
	if(lngE > lngM > lngS):
		return True
	if(latE < latM < latS):
		return True
	if(latE < latM < latS):
		return True
	return False

# rua proxima a uma coordenada
def getCloseStreet(lat, lng):
	response = requests.get("https://roads.googleapis.com/v1/nearestRoads?points=" + str(lat) + "," + str(lng) + "&key=AIzaSyCgFqHjzxrEUxKZV9uDsF3X6cYHNfEt2Do")
	closeStreets = json.loads(response.content)
	if(len(closeStreets['snappedPoints']) > 0):
		return closeStreets['snappedPoints'][0]['location']
	else:
		return None

# algoritmo principal
def start():
	stops = getStops()

	currentPosition = {
		'lat': -4.968599079455263,
		'long': -39.02392248408373
	}

	while len(stops) > 0:
		destiny = getShorterDistance(currentPosition['lat'], currentPosition['long'], stops)
		destinyStreet = getCloseStreet(destiny['lat'], destiny['long'])
			
		response = sendCoordinates(destinyStreet['latitude'], destinyStreet['longitude'])
		print(response.content)

		stops.remove(destiny)
		currentPosition = destiny

# let's go #
start()
